FROM fluent/fluentd:latest
USER root
WORKDIR /home/ubuntu
ENV PATH /home/ubuntu/ruby/bin:$PATH
RUN fluent-gem install syslog_protocol excon elasticsearch docker-api aws-sdk fluent-plugin-s3
RUN fluent-gem install faraday_middleware-aws-signers-v4 -v '0.1.1'
CMD fluentd -c /fluentd/etc/$FLUENTD_CONF -p /fluentd/plugins $FLUENTD_OPT
