module Fluent
  # NestedParserFilter
  # Author: Lukas Dolezal (lukas@pronto.co.uk)
  #
  # Allows to parse different type/formats of messages
  # by specific parsers based on matching original event values
  #
  # Example use:
  # <filter docker.var.lib.docker.containers.*.*.log>
  #    type nested_parser
  #    <match field.in.original.message value>
  #       log json lob_object
  #       message regexp message_object /(?<message_id>[0-9]*/
  #    </match> 
  # </filter>
  # 
  # This will add to all messages tagged "docker.var.lib.docker.containers.*.*.log"
  # - "log_object" that will be object parsed from JSON string in original "log" field
  # - "message_object" that will be object containing named matches of regexp (here {message_id: ".."})
  class NestedParserFilter < Filter
    Fluent::Plugin.register_filter('nested_parser', self)

    # Matcher compares input event value in given field
    # and send event to parser if matches
    class Matcher
      attr_accessor :parser

      def initialize(path, value, parser)
        @key_path = path
        @val = value
        @parser = parser
      end

      def matches?(tag, record)
        field_value(record) == @val
      end

      protected

      def field_value(record)
        @key_path.reduce(record) { |a, e| a.key?(e) ? a[e] : a[e.to_sym] }
      rescue
        nil
      end
    end

    # Base cass for parsers
    class Parser
      def transform(record)
        old_value = field_value(record)
        return record unless old_value

        v = new_value(old_value)
        return record unless v

        merge_new_value(record, v)
      end

      protected

      def initialize(field_path, merge_path)
        @field_path = field_path
        @merge_path = merge_path
      end

      def field_value(record)
        @field_path.reduce(record) { |a, e| a.key?(e) ? a[e] : a[e.to_sym] }
      rescue
        nil
      end

      def merge_new_value(record, new_value)
        new_record = record.dup
        field_parent = @merge_path[0..-2].reduce(new_record) { |a, e| a.key?(e) ? a[e] : a[e.to_sym] }
        return record unless field_parent

        key = @merge_path.last
        key = field_parent.key?(key) ? key : key.to_sym
        field_parent[key] = new_value
        new_record
      end
    end

    # <match ...>
    #   field json output_field
    # </match>
    # parses given field using JSON decoder and replaces the field with result
    class JSONParser < Parser
      def initialize(field, params)
        super field.split('.'), params.split('.')
      end

      def new_value(old_value)
        JSON.parse(old_value)
      rescue
        nil
      end
    end

    # <match ...>
    #   field regexp output_field regular_expresion
    # </match>
    # parses given field using given regular expression and replaces field with
    # hash containing expression named groups
    class RegExpParser < Parser
      def initialize(field, params)
        out_field, regexp = params.split(' ', 2)
        super field.split('.'), out_field.split('.')

        @regexp = create_regexp(regexp)
      end

      def new_value(old_value)
        match = @regexp.match(old_value.strip)
        return nil unless match

        match.names.map { |name| [name, match[name]] }.to_h
      end

      protected

      def create_regexp(regexp_string)
        regexp_string = regexp_string[1..-1] if regexp_string[0] == '/'
        regexp_string = regexp_string[0..-2] if regexp_string[-1] == '/'

        Regexp.compile(regexp_string)
      end
    end


    def configure(conf)
      super
      # parse elements
      # <match field value>
      #   field type params
      # </match>
      @matchers = []
      conf.elements.select { |element| element.name == 'when' }.each do |element|
        key, value = element.arg.split(' ', 2)
        key = key.split('.')

        in_field = element.each_pair.first[0]
        parser_type, parser_params = element.each_pair.first[1].split(' ', 2)

        element.has_key?(in_field) # record use of the key to stop fluentd unused key complains
        parser = create_parser(parser_type, in_field, parser_params)

        matcher = Matcher.new(key, value, parser)
        @matchers << matcher
      end
    end

    def start
      super
      # This is the first method to be called when it starts running
      # Use it to allocate resources, etc.
    end

    def shutdown
      super
      # This method is called when Fluentd is shutting down.
      # Use it to free up resources, etc.
    end

    def filter(tag, time, record)
      match = @matchers.find { |m| m.matches?(tag, record) }
      return record unless match

      new_record = match.parser.transform(record)
      #puts "nested_parser new record: #{new_record}"
      new_record
    end

    protected

    # parser facotry
    def create_parser(type, in_field, params)
      case type
      when 'json'
        JSONParser.new(in_field, params)
      when 'regexp'
        RegExpParser.new(in_field, params)
      else
        raise Fluent::ConfigError, "Unknown nested_parser type `#{type}`. Known types: json"
      end
    end
  end
end
