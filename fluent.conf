<source>
  type tail
  path /var/lib/docker/containers/*/*-json.log
  pos_file /var/fluent/fluentd-docker.pos
  time_format %Y-%m-%dT%H:%M:%S
  tag docker.*
  format json
  read_from_head true
</source>

<filter docker.var.lib.docker.containers.*.*.log>
  type docker_metadata
</filter>

<filter docker.var.lib.docker.containers.*.*.log>
  type nested_parser
  <when docker.labels.fluentd-log-format json>
    log json lob
  </when>
  <when docker.labels.fluentd-log-format nginx>
    log regexp nginx_access /^(?<remote>[^ ]*) (?<host>[^ ]*) (?<user>[^ ]*) \[(?<time>[^\]]*)\] "(?<method>\S+)(?: +(?<path>[^\"]*) +\S*)?" (?<code>[^ ]*) (?<size>[^ ]*)(?: "(?<referer>[^\"]*)" "(?<agent>[^\"]*)")?( "(?<gzip_ratio>[^\"]*)")?$/
  </when>
  <when docker.labels.fluentd-log-format osrm>
    log regexp osrm /^\[(?<level>[^\]]*)\] (?<time>[^ ]+ [^ ]+) (?<remote>[^ ]+) - (?<agent>[^ ]+) (?<path>[^?]*)\??(?<params>[^ ]*)?$/
  </when>
</filter>

<filter docker.**>
  type deep_grep
  exclude1 lob.user_agent ^ELB\-HealthChecker
  exclude2 nginx_access.agent ^ELB\-HealthChecker
  exclude3 osrm.agent ^ELB\-HealthChecker
</filter>

<match docker.**>
  type copy
  <store>
    type aws-elasticsearch-service
    logstash_format true
    type_name_key docker.labels.fluentd-log-type
    flush_interval 5s
    reload_connections false
    <endpoint>
      url "#{ENV['ES_URL']}"
      region "#{ENV['ES_REGION']}"
      access_key_id "#{ENV['ACCESS_ID']}"
      secret_access_key "#{ENV['ACCESS_KEY']}"
    </endpoint>
  </store>
  <store>
    type s3

    aws_key_id "#{ENV['ACCESS_ID']}"
    aws_sec_key "#{ENV['ACCESS_KEY']}"
    s3_bucket "#{ENV['S3_BUCKET_NAME']}"
    s3_region "#{ENV['ES_REGION']}"
    s3_object_key_format %{path}/%{time_slice}_%{index}.%{file_extension}
    path logs
    time_slice_format %Y/%m/%d/%Y%m%d
    buffer_path /var/fluent/s3
    storage_class STANDARD_IA

    buffer_chunk_limit 1024m

    time_slice_wait 1h
    utc
  </store>
</match>

